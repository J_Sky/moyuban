/**
 * 返回两个日期之间的天数
 * @param {*} data1 
 * @param {*} data2 
 * @returns 
 */
function caltime(data1, data2) {
    // 将日期字符串转换为Date对象
    let date1 = new Date(data1);
    let date2 = new Date(data2);
    // 计算两个日期的时间戳差值
    let timeDiff = Math.abs(date2.getTime() - date1.getTime());
    // 将时间戳转换为天数
    let dayDiff = Math.ceil(timeDiff / (1000 * 3600 * 24));
    // 返回天数差值
    return dayDiff;
}
function getLastSunday() {
    let today = new Date(); // 获取当前日期
    let dayOfWeek = today.getDay(); // 获取今天是星期几，0 表示周日，1 表示周一，以此类推
    let daysToSunday = 7 - dayOfWeek; // 计算距离下一个周日还有多少天
    let lastSunday = new Date(today.getTime() + daysToSunday * 24 * 60 * 60 * 1000); // 将日期向后推迟相应的天数
    return lastSunday.getFullYear() + '-' + (lastSunday.getMonth() + 1) + '-' + lastSunday.getDate(); // 将日期转换为字符串格式并返回，注意月份需要加 1
}

/**
 * 
 * @param {*} date 
 * @returns 返回 当前的日期 和星期几
 */
function getWeekday(date) {
    let weekMap = { 0: '日', 1: '一', 2: '二', 3: '三', 4: '四', 5: '五', 6: '六' }; // 星期几和中文对应的字典
    let week = new Date(date).getDay(); // 获取日期对应的星期几，0 表示周日，1 表示周一，以此类推
    return date + ' 星期' + weekMap[week];
}

/**
 * 
 * @returns 返回今天的日期
 */
function getToday() {
    let today = new Date(); // 获取当前时间
    let year = today.getFullYear(); // 获取当前年份
    let month = today.getMonth() + 1; // 获取当前月份，注意要加 1
    let day = today.getDate(); // 获取当前日期
    return year + '-' + (month < 10 ? '0' + month : month) + '-' + (day < 10 ? '0' + day : day); // 返回格式化后的日期字符串
}

// console.log(caltime('2020-01-01', '2022-01-02'));
// console.log(getLastSaturday());
// console.log(getWeekday('2023-03-04'));
// console.log(getToday());

const holidays = [
    ['周末', getLastSunday()],
    ['愚人节', '2023-04-01'],
    ['清明节', '2023-04-05'],
    ['劳动节', '2023-05-01'],
    ['端午节', '2023-06-22'],
    ['中秋节', '2023-09-29'],
    ['国庆节', '2023-10-01'],
    ['程序员节', '2023-10-24'],
    ['圣诞节', '2023-12-25'],
    ['元旦', '2024-01-01'],
    ['春节', '2024-02-10'],
    ['情人节', '2024-02-14'],
]

let today = getToday()

let str_1 = '[摸鱼办提醒您]:今天是' + getWeekday(today) +
    '，新春伊始,万物复苏,加油吧!打工人。工作再累，一定不要忘记摸鱼哦！有事没事起身去茶水间，去厕所，去廊道走走别老在工位上坐着，钱是老板的,但命是自己的。'
console.log(str_1);
for (let i of holidays) {
    console.log(("距离" + i[0] + "还有" + caltime(today, i[1]) + "天"));
}

