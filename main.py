# -*- coding:utf-8 -*-

import time
import datetime
from datetime import timedelta


def caltime(date1, date2):
    """
    返回两个日期之间的间隔天数
    caltime('2021-10-01','2021-11-01')
    :param date1: str  2021-01-22 开始时间
    :param date2:str  2021-02-22 结束时间
    :return:int
    """
    # %Y-%m-%d为日期格式，其中的-可以用其他代替或者不写，但是要统一，同理后面的时分秒也一样；可以只计算日期，不计算时间。
    # date1=time.strptime(date1,"%Y-%m-%d %H:%M:%S")
    # date2=time.strptime(date2,"%Y-%m-%d %H:%M:%S")
    date1 = time.strptime(date1, "%Y-%m-%d")
    date2 = time.strptime(date2, "%Y-%m-%d")
    # 根据上面需要计算日期还是日期时间，来确定需要几个数组段。下标0表示年，小标1表示月，依次类推...
    # date1=datetime.datetime(date1[0],date1[1],date1[2],date1[3],date1[4],date1[5])
    # date2=datetime.datetime(date2[0],date2[1],date2[2],date2[3],date2[4],date2[5])
    date1 = datetime.datetime(date1[0], date1[1], date1[2])
    date2 = datetime.datetime(date2[0], date2[1], date2[2])
    # 返回两个变量相差的值，就是相差天数
    # print((date2-date1).days)#将天数转成int型
    return (date2 - date1).days


def retYearMonth():
    """
    返回一个年月的字符串
    """
    y = datetime.datetime.now().strftime("%Y")  # 现在的年份
    m = datetime.datetime.now().strftime("%m")  # 现在的月份

    return "{}-{}".format(y, m)


def retWeekend():
    """
    返回最近的一个周六
    """
    now = datetime.datetime.now()
    this_week_end = now + timedelta(days=6 - now.weekday())
    return str(this_week_end.year) + "-" + str(this_week_end.month) + "-" + str(this_week_end.day)


def get_weekday(date):
    week_map = {0: '一', 1: '二', 2: '三', 3: '四', 4: '五', 5: '六', 6: '日'}
    week = datetime.datetime.strptime(date, '%Y-%m-%d').weekday()
    return f"{date} 星期{week_map[week]}"


# 今天的日期
today = (str(datetime.datetime.now().year) + "-" + str(datetime.datetime.now().month) + "-" + str(
    datetime.datetime.now().day))

holidays = [
    ['周末', retWeekend()],
    ['愚人节', '2023-04-01'],
    ['清明节', '2023-04-05'],
    ['劳动节', '2023-05-01'],
    ['端午节', '2023-06-22'],
    ['中秋节', '2023-09-29'],
    ['国庆节', '2023-10-01'],
    ['程序员节', '2023-10-24'],
    ['圣诞节', '2023-12-25'],
    ['元旦', '2024-01-01'],
    ['春节', '2024-02-10'],
    ['情人节', '2024-02-14'],
]


def main():
    """
    程序入口
    """
    # str_1 = '[摸鱼办提醒您]:今天是' + get_weekday(today) + '，羊了刚好，注意保暖，少喝凉水。工作再累，一定不要忘记摸鱼哦！有事没事起身去茶水间，去厕所，去廊道走走别老在工位上坐着，钱是老板的,但命是自己的。'
    str_1 ='[摸鱼办提醒您]:今天是'+get_weekday(today)+'，新春伊始,万物复苏,加油吧!打工人。工作再累，一定不要忘记摸鱼哦！有事没事起身去茶水间，去厕所，去廊道走走别老在工位上坐着，钱是老板的,但命是自己的。'
    # str_1 = '[摸鱼办提醒您]:今天是' + get_weekday(today) + ',夏季炎热,谨防中暑,加油!打工人。工作再累，一定不要忘记摸鱼哦！有事没事起身去茶水间，去厕所，去廊道走走别老在工位上坐着，钱是老板的,但命是自己的。'
    str_3 = '认认真真上班，这根本就不叫赚钱，那是用劳动换取报酬。只有偷懒，在上班的时候摸鱼划水，你才是从老板手里赚到了钱。最后，祝愿天下所有摸鱼人都能愉快的渡过每一天！'
    print(str_1)
    for i in holidays:
        print("距离" + i[0] + "还有" + str(caltime(today, i[1])) + "天")
    print(str_3)


if __name__ == '__main__':
    main()
